/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.Candidatura;
import Geral.CentroExposições;
import Geral.Exposição;
import Geral.RegistoExposições;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class CriarCandidaturasController {
    private CentroExposições cec;
    private Exposição nova_candidatura;
    private Candidatura cand;
    private RegistoExposições re;
    
    public CriarCandidaturasController(CentroExposições ce){
        this.cec=ce;
    }
    public ArrayList<Exposição> getListaExposições(){
         return this.cec.getListaExposições();
    }
     public String mostraDados(){
       return this.nova_candidatura.toString();
    }
      public String mostraDados2(){
       return this.cand.toString();
    }
     public void selecionaExposição(Exposição exp) {
        this.nova_candidatura=exp;
    }
     public void NovaCandidatura(String nomeComercial,String morada,String tlm,String area){
         this.cand = this.nova_candidatura.NovaCandiatura();
         this.cand.setNomeComercial(nomeComercial);
         this.cand.setMorada(morada);
         this.cand.settlm(tlm);
         this.cand.setArea(area);
     }
     public boolean registaCandidatura()
    {
        return this.nova_candidatura.registaCandidatura(cand);
    }
}
