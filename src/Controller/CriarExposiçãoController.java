/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.CentroExposições;
import Geral.*;

import java.util.ArrayList;


/**
 *
 * @author Joao
 */
public class CriarExposiçãoController {
private final CentroExposições cec;
    private Exposição nova_exposição;
    private RegistoExposições re;
    
    
    public CriarExposiçãoController(CentroExposições ce){
        this.cec=ce;
    }
    public void NovaExposição(String titulo,String descritivo,String local,String data){
        this.nova_exposição=this.cec.NovaExposição();
        this.nova_exposição.setTitulo(titulo);
        this.nova_exposição.setDescritivo(descritivo);
        this.nova_exposição.setLocal(local);
        this.nova_exposição.setData(data);
        
    }
    public ArrayList<Utilizador> getListaUtilizadores(){
         return this.cec.getListaUtilizadores();
    }

    public void addOrganizador(Utilizador u){
        
        this.nova_exposição.addOrganizador(u);
    }
    
    public boolean registaExposicão()
    {
        return this.cec.registaExposição(nova_exposição);
    }
    public String mostraDados(){
       return this.nova_exposição.toString();
    }
    public boolean ValidaQuantidade(){
        if(nova_exposição.contOrganizador>1 ){
            return true;
        }else{
            return false;
        }
    }

  
    
}
