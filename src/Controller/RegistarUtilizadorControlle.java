/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.CentroExposições;
import Geral.Utilizador;

/**
 *
 * @author Joao
 */
public class RegistarUtilizadorControlle {
    private CentroExposições cec;
    private Utilizador uti;
    
    
     public RegistarUtilizadorControlle(CentroExposições ce){
        this.cec=ce;
    }

     public void NovoUT(String nome,String email,String username,String pass){
        this.uti=this.cec.NovoUtilizador();
        this.uti.setNome(nome);
        this.uti.setEmail(email);
        this.uti.setUsername(username);
        this.uti.setPass(pass);
        
    }
     
    public String mostraDados() {
       return this.uti.toString();
    }
     public boolean registaUtilizador()
    {
        return this.cec.registaUtilizador(uti);
    }
}
