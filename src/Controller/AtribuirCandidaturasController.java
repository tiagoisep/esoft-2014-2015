/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.Candidatura;
import Geral.CentroExposições;
import Geral.Decisão;
import Geral.Exposição;
import Geral.*;
import Geral.Mecanismos;
import Geral.Utilizador;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class AtribuirCandidaturasController {
    private CentroExposições cec;
    private Exposição exposi;
    private Candidatura candit;
    private Decisão deci;
    private Mecanismos mec;
    private RegistoExposições re;
    private RegistoUtilizadores ru;
    private RegistoCandidaturas ra;
    
    public AtribuirCandidaturasController(CentroExposições ce){
        this.cec=ce;
    }
    public AtribuirCandidaturasController(Exposição atrib_candidatura){
        this.exposi=atrib_candidatura;
        this.ra=new RegistoCandidaturas();
    }
    
    /////////////////////////////////////////////
    public ArrayList<Mecanismos> getListaMecanismos(){
        return this.cec.lMecanismos;
    }
    
    public void selecionaMecanismos(Mecanismos mec){
        this.mec=mec;
    }
    ///////////////////////////////////////////////////
    public String mostraDados(){
       return this.exposi.toString();
    }

    public void selecionaExposição(Exposição exp) {
        this.exposi=exp;
    }

    public ArrayList<Candidatura> getListaCandidaturas() {
        return this.ra.lCandidaturas;
    }

    public void selecionaCandidatura(Candidatura candi) {
        this.candit=candi;
    }

    public String mostraDados2() {
        return this.candit.toString();
    }

    public ArrayList<Utilizador> getListaFae() {
        return this.cec.getListaUtilizadores();
    }
    
    public ArrayList<Exposição> getListaExposições(){
         return this.cec.getListaExposições();
    }
    


    
}
