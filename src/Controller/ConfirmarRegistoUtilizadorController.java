/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.CentroExposições;
import Geral.*;
import Geral.Utilizador;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class ConfirmarRegistoUtilizadorController {
     private CentroExposições cec;
     private Exposição expo;
     private Utilizador ut;
     private RegistoExposições re;
    

    public ConfirmarRegistoUtilizadorController(CentroExposições ce) {
         this.cec=ce;
    }

    public ArrayList<Exposição> getListaExposições() {
        return this.cec.getListaExposições();
    }

    public void selecionaExposição(Exposição exposição) {
       this.expo=exposição;
    }

    public ArrayList<Utilizador> getListaUtilizadores() {
        return this.cec.getListaUtilizadores();
    }

    public boolean confirmaRegistoUtilizador() {
        return this.cec.registaUtilizador(ut);
    }

    public String mostraDados() {
         return this.ut.toString();
    }

    public void SelecionaUtilizador(Utilizador utilizador) {
        this.ut=utilizador;
    }
    
}
