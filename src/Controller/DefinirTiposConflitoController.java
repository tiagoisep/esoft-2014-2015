/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.*;

/**
 *
 * @author Joao
 */
public class DefinirTiposConflitoController {
    private CentroExposições cec;
    private RegistoConflitos rc;
    private Conflito c;
    
    public DefinirTiposConflitoController(CentroExposições ce){
        this.cec=ce;
        this.rc=new RegistoConflitos();
        this.c=new Conflito();
    }

    public boolean registaConflito() {
       return rc.addConflito(c);
    }

    public String mostraDados() {
       return c.toString();
    }

    public void defineTipoConflito(String conflito) {
        c.setTipo(conflito);
    }
}
