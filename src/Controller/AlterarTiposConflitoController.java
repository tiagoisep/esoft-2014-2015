/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.*;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class AlterarTiposConflitoController {
        private CentroExposições cec;
        private RegistoConflitos rc;
        private Conflito c;
    
     public AlterarTiposConflitoController(CentroExposições ce){
        this.cec=ce;
        this.rc=new RegistoConflitos();
//        this.c=new Conflito();
    }

    public ArrayList<Conflito> getListaConflitos() {
        return this.rc.lConflitos;
    }

    public void selecionaConflito(Conflito conf) {
        this.c=conf;
    }

    public String mostraDados() {
        return this.c.toString();
    }

    public boolean registaConflito() {
        return this.rc.registaConflito(c);
    }

    public void removeConflito() {
        rc.removeConflito(c);
    }

    public void alteraConflito(String tipo) {
        c.setTipo(tipo);
    }
     
     
}
