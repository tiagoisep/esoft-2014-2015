/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.*;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class AlterarCandidaturaController {
     private CentroExposições cec;
     private Exposição exposi;
     private Candidatura candit;
     private RegistoCandidaturas ra;
     
     public AlterarCandidaturaController(CentroExposições ce){
        this.cec=ce;
        this.exposi=new Exposição();
        this.ra=new RegistoCandidaturas ();
    }
     
     public ArrayList<Candidatura> getListaCandidaturas() {
        return this.ra.lCandidaturas;
    }
     public void selecionaCandidatura(Candidatura candi) {
        this.candit=candi;
    }
     public String mostraDados2() {
        return this.candit.toString();
    }
     public void CandidaturaAlterada(String nomeComercial,String morada,String tlm,String area){
         this.candit.setNomeComercial(nomeComercial);
         this.candit.setMorada(morada);
         this.candit.settlm(tlm);
         this.candit.setArea(area);
     }
     public boolean registaCandidatura()
    {
        return this.exposi.registaCandidatura(candit);
    }
}
