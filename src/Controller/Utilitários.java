/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.Exposição;
import Geral.Utilizador;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Joao
 */
public class Utilitários {
    

    public static void listar(ArrayList<Utilizador> lisUt) {
        
            System.out.println(lisUt.toString() +"\n");
        
    }


    public static void listar2(ArrayList<Exposição> lisExp) {
        System.out.println(lisExp.toString() +"\n");
    }

    public static Object apresentaESeleciona(ArrayList lisUt, String sHeader) {
        {
        apresentaLista(lisUt,sHeader);
        return selecionaObject(lisUt);
    }
    }
     static public void apresentaLista(List list,String sHeader)
    {
        System.out.println(sHeader);

        int index = 0;
        for (Object o : list)
        {
            index++;

            System.out.println(index + ". " + o.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }
     static public Object selecionaObject(List list)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utilitários.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > list.size());

        if (nOpcao == 0)
        {
            return null;
        } else
        {
            return list.get(nOpcao - 1);
        }
    }
      static public String readLineFromConsole(String strPrompt)
    {
        try
        {
            System.out.println(strPrompt);

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            return in.readLine();
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
         static public boolean confirma(String sMessage) {
        String strConfirma;
        do {
            strConfirma = Utilitários.readLineFromConsole("\n" + sMessage + "\n");
        } while (!strConfirma.equalsIgnoreCase("s") && !strConfirma.equalsIgnoreCase("n"));

        return strConfirma.equalsIgnoreCase("s");
    }
}
