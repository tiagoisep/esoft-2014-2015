/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.*;
import java.util.ArrayList;


/**
 *
 * @author Joao
 */
public class AlterarPerfilUtilizadorController {
    private CentroExposições cec;
    

    public AlterarPerfilUtilizadorController(CentroExposições ce) {
        this.cec=ce;
    }
    
     public ArrayList<Utilizador> getListaUtilizadores(){
         return this.cec.getListaUtilizadores();
    }
     public void alteraUT(Utilizador u,String nome,String email,String username,String pass){
//        u=this.cec.NovoUtilizador();
        u.setNome(nome);
        u.setEmail(email);
        u.setUsername(username);
        u.setPass(pass);
        
    }
     public String mostraDados(Utilizador u) {
       return u.toString();
    }
     
     public boolean registaUtilizador(Utilizador u)
    {
        return this.cec.registaUtilizador(u);
    }
}
