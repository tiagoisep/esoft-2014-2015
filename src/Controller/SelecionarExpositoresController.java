/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.Candidatura;
import Geral.CentroExposições;
import Geral.Decisão;
import Geral.Exposição;
import Geral.RegistoCandidaturas;
import Geral.RegistoExposições;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class SelecionarExpositoresController {
     private CentroExposições cec;
    private Exposição expo;
      private Candidatura candit;
      private Decisão dec;
      private RegistoExposições re;
      private RegistoCandidaturas ra;
      
    public SelecionarExpositoresController(CentroExposições ce){
        this.cec=ce;
        this.ra=new RegistoCandidaturas();
    }

   public ArrayList<Exposição> getListaExposições(){
         return this.cec.getListaExposições();
    }

     public void selecionaExposição(Exposição exp) {
        this.expo=exp;
    }
       public String mostraDados(){
       return this.dec.toString();
    }

    public ArrayList<Candidatura> getListaCandidaturas() {
        return this.ra.lCandidaturas;
    }

    public void selecionaCandidatura(Candidatura candi) {
        this.candit=candi;
    }

    public String mostraDados2() {
        return this.candit.toString();
    }

    public void NovaDecisão(String decisão, String explica) {
        this.dec=this.expo.NovaDecisão();
        this.dec.setDecisão(decisão);
        this.dec.setExplicação(explica);
    }
     
}
