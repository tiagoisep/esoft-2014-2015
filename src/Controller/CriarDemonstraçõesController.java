/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.CentroExposições;
import Geral.Demonstrações;
import Geral.Exposição;
import Geral.ListaRecursos;
import Geral.Recurso;
import Geral.RegistoExposições;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class CriarDemonstraçõesController {
    private CentroExposições cec;
    private Exposição exposi;
    private Demonstrações demo;
    private Recurso rec;
    private RegistoExposições re;
    private ListaRecursos lr;
    
     public void NovaDemonstração(){
        this.demo=this.cec.NovaDemonstração();     
    }
    
    public CriarDemonstraçõesController(CentroExposições ce){
        this.cec=ce;
        this.lr=new ListaRecursos();
    }
    
    public ArrayList<Exposição> getListaExposições(){
         return this.cec.getListaExposições();
    }
    
    public String mostraDados(){
       return this.exposi.toString();
    }

    public void selecionaExposição(Exposição exp) {
        this.exposi=exp;
    }
    
     public ArrayList<Recurso> getListaRecursos(){
         return this.lr.lRecurso;
    }
    
     public void addRecurso(Recurso r){       
        this.demo.addRecurso(r);
    }
     public String mostraDados2(){
       return this.demo.toString();
    }
}
