/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Geral.CentroExposições;
import Geral.Exposição;
import Geral.Utilizador;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class DefinirFAEController {
    private CentroExposições cec;
    private Exposição novo_fae;
    
    public DefinirFAEController(CentroExposições ce){
        this.cec=ce;
    }
    public DefinirFAEController(Exposição novo_fae){
        this.novo_fae=novo_fae;
        
    }
    
    public ArrayList<Exposição> getListaExposições(){
         return this.cec.getListaExposições();
    }
    public ArrayList<Utilizador> getListaUtilizadores(){
         return this.cec.getListaUtilizadores();
    }

    public void addFae(Utilizador u){
        this.novo_fae.addFae(u);
    }
     public String mostraDados(){
       return this.novo_fae.toString();
    }

    public void selecionaExposição(Exposição exp) {
        this.novo_fae=exp;
    }
   
}
