/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.ConfirmarRegistoUtilizadorController;
import Controller.Utilitários;
import Geral.CentroExposições;
import Geral.Exposição;
import Geral.Utilizador;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class ConfirmarRegistoUtilizadorUI {
    private final ConfirmarRegistoUtilizadorController uc7_Controller;
    
    public ConfirmarRegistoUtilizadorUI(CentroExposições ce){
        this.uc7_Controller=new ConfirmarRegistoUtilizadorController(ce);
    }
    Object exp =null;
    Object ut =null;
    public void run(){
        System.out.println("Lista de exposições das quais é Gestor");
        
        ArrayList<Exposição> lisExp = uc7_Controller.getListaExposições();
        ArrayList<Utilizador> lisUt =uc7_Controller.getListaUtilizadores();
        System.out.println("Pf selecione uma exposição\n" );

                 exp = Utilitários.apresentaESeleciona(lisExp, "Selecione Exposição");
                 if (exp != null)
                 uc7_Controller.selecionaExposição((Exposição)exp);
                 
                 System.out.println("Pedidos de registo de Utilizadores à espera de confirmação"  );

                 ut = Utilitários.apresentaESeleciona(lisUt, "Selecione Utilizador -> Confirme");
                 if (ut != null)
                 uc7_Controller.SelecionaUtilizador((Utilizador) ut);
                 System.out.println("Por favor confirme os dados\n" +uc7_Controller.mostraDados() );
                 
      if (Utilitários.confirma("Confirma os dados do registo de utilizador? (S/N)")) {
            if (uc7_Controller.confirmaRegistoUtilizador()) {
                System.out.println("Registo criado com sucesso.");
            } else {
                System.out.println("Não foi possivel registar corretamente o Utilizador.");
            }
        }
      
    }             
                 
}
