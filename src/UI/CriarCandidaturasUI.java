/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.CriarCandidaturasController;
import Controller.DefinirFAEController;
import Controller.Utilitários;
import Geral.CentroExposições;
import Geral.Exposição;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Joao
 */
public class CriarCandidaturasUI {
    private final CriarCandidaturasController uc5_Controller;
    
        public CriarCandidaturasUI(CentroExposições ce){
        this.uc5_Controller=new CriarCandidaturasController(ce);
    }
    
         Object exp =null;
    public void run(){
        System.out.println("Lista de exposições disponíveis");
        
        ArrayList<Exposição> lisExp = uc5_Controller.getListaExposições();
        
        
        System.out.println("Pf selecione uma exposição\n" );

                 exp = Utilitários.apresentaESeleciona(lisExp, "Selecione Exposição");
                 if (exp != null)
                 uc5_Controller.selecionaExposição((Exposição)exp);
        
      System.out.println("Selecionou a exposição  \n" +uc5_Controller.mostraDados() );
      
      System.out.println("Pf insira os dados da candidatura" );
      Scanner in=new Scanner(System.in);
    System.out.println("Formulário de Candidatura");
    System.out.println("Introduza Nome comercial da entidade que representa");
    String nomeComercial=in.nextLine();
    System.out.println("Introduza a Morada");
    String morada=in.nextLine();
    System.out.println("Introduza o Telemóvel");
    String tlm=in.nextLine();
    System.out.println("Introduza a área de exposição pretendida");
    String area=in.nextLine();
    
    uc5_Controller.NovaCandidatura(nomeComercial,morada,tlm,area);
    
     System.out.println("Por favor confirme os dados\n" +uc5_Controller.mostraDados2() );
      
      
     if (Utilitários.confirma("Confirma os dados da Candidatura? (S/N)")) {
            if (uc5_Controller.registaCandidatura()) {
                System.out.println("Candidatura submetida com sucesso.");
            } else {
                System.out.println("Não foi possivel submeter corretamente a Candidatura.");
            }
        }
}

    
}
