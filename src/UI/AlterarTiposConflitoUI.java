/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.*;
import Geral.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Joao
 */
public class AlterarTiposConflitoUI {
     private final AlterarTiposConflitoController uc14_Controller;
    
        public AlterarTiposConflitoUI(CentroExposições ce){
        this.uc14_Controller=new AlterarTiposConflitoController(ce);
    }
        Scanner in = new Scanner(System.in);
        Object conf =null;
        void run(){
            System.out.println("\nA sua lista de candidaturas\n");
      
      ArrayList<Conflito> lisConf = uc14_Controller.getListaConflitos();
      
       conf = Utilitários.apresentaESeleciona(lisConf, "\nSelecione Candidatura\n");
                 if (conf != null)
                 uc14_Controller.selecionaConflito((Conflito)conf);
                 
       System.out.println("Selecionou o Conflito  \n" +uc14_Controller.mostraDados() );
       
        String menu="Que pretende fazer?\n1-Eleminar o Conflito?\n2-Editar o Conflito\n0-terminar";
        int op;
        do {
        System.out.println(menu);  
        op = in.nextInt();
        switch (op) {
            case(1):
                in.nextLine();
                System.out.println("Tem a certeza que pretende eleminar este conflito? (S/N)");
                String confirma=in.nextLine();
                if(confirma.equalsIgnoreCase("s")){
                    uc14_Controller.removeConflito();
                    System.out.println("Conflito removido");
                }else{
                    System.out.println("Não foi possivel proceder às alterações");
                }
                
                break;
                
            case (2):
                in.nextLine();
                System.out.println("Introduza o novo tipo de conflito pff");
                String tipo = in.nextLine();
                
                System.out.println(uc14_Controller.mostraDados() +" foi alterado para " +tipo +"\nConfirma os dados ? (S/N)" );
                confirma=in.nextLine();
                if(confirma.equalsIgnoreCase("s")){
                    uc14_Controller.alteraConflito(tipo);
                }else{
                    System.out.println("Não foi possivel proceder às alterações");
                }

               
        
                break;
       
       default:
       if(op!=0){
        System.out.println("Opção inválida!!");
        }
        }
         } while (op != 0);
       
        }
}
