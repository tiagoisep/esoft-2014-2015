/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.CriarExposiçãoController;
import Controller.Utilitários;
import Geral.CentroExposições;
import Geral.Exposição;
import Geral.Utilizador;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Joao
 */
public class CriarExposiçãoUI {
    private final CriarExposiçãoController uc_controller;
    
    public CriarExposiçãoUI(CentroExposições ce){
        this.uc_controller = new CriarExposiçãoController(ce);
    }
    public void run(){
        Scanner in=new Scanner(System.in);
    System.out.println("Nova Exposição");
    System.out.println("Introduza titulo");
    String titulo=in.nextLine();
    System.out.println("Introduza o uma pequena descrição");
    String descritivo=in.nextLine();
    System.out.println("Introduza o Local");
    String local=in.nextLine();
    System.out.println("Introduza a data");
    String data=in.nextLine();
    
    uc_controller.NovaExposição(titulo,descritivo,local,data);
    ArrayList<Utilizador> lisUt = uc_controller.getListaUtilizadores();
    Object ut = null;
    Utilitários.listar(lisUt);
        
    
    do{
        System.out.println("Pf indique o username do utilizador que deseja designar como Organizador\n" );

                 ut = Utilitários.apresentaESeleciona(lisUt, "Selecione Utilizador -> Organizador");
                 if (ut != null)
                 uc_controller.addOrganizador((Utilizador)ut);      
         }while(ut != null );
    if(uc_controller.ValidaQuantidade() == true){
      System.out.println("Por favor confirme os dados\n" +uc_controller.mostraDados() );
      
      
     if (Utilitários.confirma("Confirma os dados da exposição? (S/N)")) {
            if (uc_controller.registaExposicão()) {
                System.out.println("Exposição criada com sucesso.");
            } else {
                System.out.println("Não foi possivel guardar corretamente a Exposição.");
            }
        }
    }else{
        System.out.println("ERRO!!!!!\nNão selecionou pelo menos dois organizadores.\n");
    }
    }
}
   

    

