/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AtribuirCandidaturasController;
import Controller.Utilitários;
import Geral.Candidatura;
import Geral.CentroExposições;
import Geral.Exposição;
import Geral.FAE;
import Geral.Mecanismos;
import Geral.Utilizador;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class AtribuirCandidaturasUI {
    
     private final AtribuirCandidaturasController uc3_Controller;
    
    public AtribuirCandidaturasUI(CentroExposições ce){
        this.uc3_Controller=new AtribuirCandidaturasController(ce);
    }
     Object exp =null;
     Object candi =null;
     Object fae=null;
     Object mec=null;

    void run() {
        System.out.println("\nLista de exposições das quais é Organizador\n");
        
        ArrayList<Exposição> lisExp = uc3_Controller.getListaExposições();
                 exp = Utilitários.apresentaESeleciona(lisExp, "\nSelecione Exposição");
                 if (exp != null)
                 uc3_Controller.selecionaExposição((Exposição)exp);
         
      System.out.println("\nSelecionou a exposição  \n" +uc3_Controller.mostraDados() );
     //////////////////////////////////////////////////////////////////////////////////////////
      
      System.out.println("\nLista de mecanismos de atribuição disponiveis\n");
      ArrayList<Mecanismos> lisMec = uc3_Controller.getListaMecanismos();
                    mec =Utilitários.apresentaESeleciona(lisMec, "\nSelecione um Mecanismo");
                    if(mec !=null)
                    uc3_Controller.selecionaMecanismos((Mecanismos)mec);
       System.out.println("Mecanismos não disponiveis");
                    
//      System.out.println("\nLista de candidaturas à exposição selecionada\n");
      
//      ArrayList<Candidatura> lisCand = uc3_Controller.getListaCandidaturas();
        
//       candi = Utilitários.apresentaESeleciona(lisCand, "\nSelecione Candidatura\n");
//                 if (candi != null)
//                 uc3_Controller.selecionaCandidatura((Candidatura)candi);
//                 
//       System.out.println("Selecionou a Candidatura  \n" +uc3_Controller.mostraDados2() );
//       
//       System.out.println("\nLista de FAE disponiveis\n");
       
//       ArrayList<Utilizador> lisFae = uc3_Controller.getListaFae();
      //
     
//     do{
//        System.out.println("Pf indique o fae que deseja associar\n"  );
//
//                 fae = Utilitários.apresentaESeleciona(lisFae, "Selecione FAE -> Candidatura");
//                 if(fae != null){
//                 uc3_Controller.addAssociação((Utilizador)fae);
//                 }
//         }while(fae != null);
       
     //System.out.println("Por favor confirme os dados\n" +uc3_Controller.mostraDados() );
     
//      if (Utilitários.confirma("Confirma a lista dos FAE associados à candidatura? (S/N)")) {
//            
//                System.out.println("FAE adicionados com sucesso à candidatura.");
//            } else {
//                System.out.println("Não foi possivel associar corretamente a lista dos FAE à candidatura.");
//            }
        }
       
    
}
