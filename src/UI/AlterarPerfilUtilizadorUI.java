/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AlterarPerfilUtilizadorController;
import Controller.Utilitários;
import Geral.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Joao
 */
public class AlterarPerfilUtilizadorUI {
    private final AlterarPerfilUtilizadorController uc10controller;

    AlterarPerfilUtilizadorUI(CentroExposições ce) {
        this.uc10controller=new AlterarPerfilUtilizadorController(ce);
    }
    Scanner in = new Scanner(System.in);
    void run(){
        System.out.println("Introduza o seu username");
        String username=in.nextLine();
     
        ArrayList<Utilizador> lisUt = uc10controller.getListaUtilizadores();
        int existe=0;
        for(Utilizador u: lisUt){
            if(username.equalsIgnoreCase(u.getUsername())){
                System.out.println("Nova Utilizador");
                System.out.println("Introduza nome");
                String nome=in.nextLine();
                System.out.println("Introduza o email");
                String email=in.nextLine();
                System.out.println("Introduza a pass");
                String pass=in.nextLine();
                
                uc10controller.alteraUT(u,nome,email,username,pass);
                
                System.out.println("Por favor confirme os dados\n" +uc10controller.mostraDados(u) );
                
                 if (Utilitários.confirma("Confirma os dados? (S/N)")) {
            if (uc10controller.registaUtilizador(u)) {
                System.out.println("Utilizador atualizado com sucesso ");
            } else {
                System.out.println("Não foi possivel guardar corretamente os dados.");
            }
        }
                 existe++;
                 break;
            }
        }
        if(existe==0){
            System.out.println("Esse username não corresponde a nenhum utilizador registado!!");
        }
                
    }
    
}
