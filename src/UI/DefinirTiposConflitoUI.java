/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;


import Controller.*;
import Geral.CentroExposições;
import java.util.Scanner;

/**
 *
 * @author Joao
 */
public class DefinirTiposConflitoUI {
     private final DefinirTiposConflitoController uc12_Controller;
    
        public DefinirTiposConflitoUI(CentroExposições ce){
        this.uc12_Controller=new DefinirTiposConflitoController(ce);
    }
        
        
        void run(){
            Scanner in= new Scanner(System.in);
            System.out.println("Tipos de Conflito");
            
            System.out.println("Pff defina os tipos de conflito\nQual o tipo de conflito?");
            String conflito=in.nextLine();
            
            uc12_Controller.defineTipoConflito(conflito);
            
            if (Utilitários.confirma("Confirma os dados? (S/N)")) {
            if (uc12_Controller.registaConflito()) {
                System.out.println("Tipo de conflito criado com sucesso.");
            } else {
                System.out.println("Não foi possivel guardar corretamente o tipo de conflito.");
            }
        }

            
        }
}
