/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Geral.CentroExposições;
import java.util.Scanner;

/**
 *
 * @author Joao
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner in=new Scanner(System.in);
        String menu="Menu\n1-Criar Exposição\n2-Definir FAE\n3-Atribuir Candidaturas\n4-Selecionar Candidaturas"
                + "         \n5-Criar Candidaturas\n6-Registar novo Utilizador\n7-Confirmar registo de Utilizador"
                + "             \n8-Criar Demonstração\n9-Definir Recursos\n10-Alterar perfil do Utilizador\n11-Alterar Candidatura\n12-Definir tipos de conflito\n13-Detetar conflitos\n14-Alterar conflitos de Interesse\n0-terminar";
         int opção=0;
           int op;
        do {
        System.out.println(menu);  
        op = in.nextInt();
        switch (op) {
        case (1):
            CentroExposições cec = new CentroExposições();
           CriarExposiçãoUI ui = new CriarExposiçãoUI(cec);
                    ui.run();
          
        break;
        case(2):
          CentroExposições cec2 = new CentroExposições();
           DefinirFAEUI ui2 = new DefinirFAEUI(cec2);
                    ui2.run();
                   
         break;
            case(3):
                CentroExposições cec3 = new CentroExposições();
           AtribuirCandidaturasUI ui3 = new AtribuirCandidaturasUI(cec3);
                    ui3.run();
                break;
                case(4):
                     CentroExposições cec4 = new CentroExposições();
           SelecionarExpositoresUI ui4 = new SelecionarExpositoresUI(cec4);
                    ui4.run();
                    break;
            case(5):
                CentroExposições cec5 = new CentroExposições();
           CriarCandidaturasUI ui5 = new CriarCandidaturasUI(cec5);
                    ui5.run();
                break;
                case(6):
                    CentroExposições cec6 = new CentroExposições();
           RegistarUtilizadorUI ui6 = new RegistarUtilizadorUI(cec6);
                    ui6.run();
                    break;
                    case(7):
                        CentroExposições cec7 = new CentroExposições();
           ConfirmarRegistoUtilizadorUI ui7 = new ConfirmarRegistoUtilizadorUI(cec7);
                    ui7.run();
                        break;
                        case(8):
                                CentroExposições cec8 = new CentroExposições();
           CriarDemonstraçõesUI ui8 = new CriarDemonstraçõesUI(cec8);
                    ui8.run();
                            break;
                             case(9):
                                CentroExposições cec9 = new CentroExposições();
           DefinirRecursosUI ui9 = new DefinirRecursosUI(cec9);
                    ui9.run();
                            break;
                             case (10):
                                CentroExposições cec10 = new CentroExposições();
                   AlterarPerfilUtilizadorUI ui10= new AlterarPerfilUtilizadorUI(cec10);
                   ui10.run();
                            break;
                           case (11):
                            CentroExposições cec11 = new CentroExposições();
                   AlterarCandidaturaUI ui11= new AlterarCandidaturaUI(cec11);
                   ui11.run();
                           break;
                           case (12):
                                     CentroExposições cec12 = new CentroExposições();
                   DefinirTiposConflitoUI ui12= new DefinirTiposConflitoUI(cec12);
                   ui12.run();
                           break;
                           case (13):
                                    System.out.println("Sistema de deteção não implementado"); 
                            break;
                            case (14):
                                       CentroExposições cec14 = new CentroExposições();
                   AlterarTiposConflitoUI ui14= new AlterarTiposConflitoUI(cec14);
                   ui14.run();
                             break;
            default:
                if(op!=0){
        System.out.println("Opção inválida!!");
        }
        }
         } while (op != 0);
    }
    
}
