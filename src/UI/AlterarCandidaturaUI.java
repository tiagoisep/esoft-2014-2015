/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.*;
import Geral.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Joao
 */
public class AlterarCandidaturaUI {
    private final AlterarCandidaturaController uc11_Controller;
    
        public AlterarCandidaturaUI(CentroExposições ce){
        this.uc11_Controller=new AlterarCandidaturaController(ce);
    }
        
        Object candi =null;
        
        void run(){
             System.out.println("\nA sua lista de candidaturas\n");
      
      ArrayList<Candidatura> lisCand = uc11_Controller.getListaCandidaturas();
        
       candi = Utilitários.apresentaESeleciona(lisCand, "\nSelecione Candidatura\n");
                 if (candi != null)
                 uc11_Controller.selecionaCandidatura((Candidatura)candi);
                 
       System.out.println("Selecionou a Candidatura  \n" +uc11_Controller.mostraDados2() );
       
                System.out.println("Pf insira os novos dados da candidatura" );
               Scanner in=new Scanner(System.in);
             System.out.println("Formulário de Candidatura");
             System.out.println("Introduza Nome comercial da entidade que representa");
             String nomeComercial=in.nextLine();
             System.out.println("Introduza a Morada");
             String morada=in.nextLine();
             System.out.println("Introduza o Telemóvel");
             String tlm=in.nextLine();
             System.out.println("Introduza a área de exposição pretendida");
             String area=in.nextLine();
             
             uc11_Controller.CandidaturaAlterada(nomeComercial,morada,tlm,area);
             
             System.out.println("Por favor confirme os dados\n" +uc11_Controller.mostraDados2() );
             
             if (Utilitários.confirma("Confirma os dados da Candidatura? (S/N)")) {
            if (uc11_Controller.registaCandidatura()) {
                System.out.println("Candidatura alterada com sucesso.");
            } else {
                System.out.println("Não foi possivel alterar corretamente a Candidatura.");
            }
        }
        }
}
