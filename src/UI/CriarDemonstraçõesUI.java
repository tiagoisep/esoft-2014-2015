/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;


import Controller.CriarDemonstraçõesController;
import Controller.Utilitários;
import Geral.CentroExposições;
import Geral.Demonstrações;
import Geral.Exposição;
import Geral.Recurso;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class CriarDemonstraçõesUI {
    private final CriarDemonstraçõesController uc8_Controller;
    
    public CriarDemonstraçõesUI(CentroExposições ce){
        this.uc8_Controller=new CriarDemonstraçõesController(ce);
    }
    
    Object exp=null;
    Object rec=null;
    void run(){
         System.out.println("\nLista de exposições das quais é Organizador\n");
        
        ArrayList<Exposição> lisExp = uc8_Controller.getListaExposições();
                 exp = Utilitários.apresentaESeleciona(lisExp, "\nSelecione Exposição");
                 if (exp != null)
                 uc8_Controller.selecionaExposição((Exposição)exp);
         
      System.out.println("\nSelecionou a exposição  \n" +uc8_Controller.mostraDados() );
      uc8_Controller.NovaDemonstração();
      do{
      System.out.println("\nLista de recursos disponiveis\n");
      ArrayList<Recurso> lisRec = uc8_Controller.getListaRecursos();
                 rec = Utilitários.apresentaESeleciona(lisRec, "\nSelecione Recursos");
                 if (rec != null)
                 uc8_Controller.addRecurso((Recurso)rec);
      }while(rec != null);  
      
      
      System.out.println("\nAdicionou os seguintes recursos à demonstração\n" +uc8_Controller.mostraDados2() );
      
      if (Utilitários.confirma("Confirma a lista de recuros associados à demonstração? (S/N)")) {
            
                System.out.println("Recursos associados com sucesso à Demonstração.");
            } else {
                System.out.println("Não foi possivel adicionar correctamente os recursos à Demonstração.");
            }
    }
    
            
}
