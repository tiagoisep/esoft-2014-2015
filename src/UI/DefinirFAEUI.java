/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.DefinirFAEController;
import Controller.Utilitários;
import Geral.CentroExposições;
import Geral.Exposição;
import Geral.Utilizador;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Joao
 */
public class DefinirFAEUI {
    private final DefinirFAEController uc2_Controller;
    
    public DefinirFAEUI(CentroExposições ce){
        this.uc2_Controller=new DefinirFAEController(ce);
    }
    Object ut =null;
   Object exp =null;
    public void run(){
        System.out.println("Lista de exposições das quais é Organizador");
        
        ArrayList<Exposição> lisExp = uc2_Controller.getListaExposições();
       ArrayList<Utilizador> lisUt =uc2_Controller.getListaUtilizadores();
        

        System.out.println("Pf selecione uma exposição\n" );

                 exp = Utilitários.apresentaESeleciona(lisExp, "Selecione Exposição");
                 if (exp != null)
                 uc2_Controller.selecionaExposição((Exposição)exp);
         
      System.out.println("Selecionou a exposição  \n" +uc2_Controller.mostraDados() );

       do{
        System.out.println("Pf indique o utilizador que deseja designar como FAE\n"  );

                 ut = Utilitários.apresentaESeleciona(lisUt, "Selecione Utilizador -> FAE");
                 if (ut != null)
                 uc2_Controller.addFae((Utilizador)ut);
                 
         }while(ut != null);
       
      System.out.println("Por favor confirme os dados\n" +uc2_Controller.mostraDados() );
      
      if (Utilitários.confirma("Confirma a lista dos FAE? (S/N)")) {
            
                System.out.println("FAE adicionados com sucesso.");
            } else {
                System.out.println("Não foi possivel definir corretamente a lista dos FAE.");
            }
        
    }                                   
      
}
