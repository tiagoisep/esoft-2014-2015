/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.*;
import Geral.*;
import static java.lang.Math.exp;
import java.util.*;


/**
 *
 * @author Tiago Ferreira
 */
public class DefinirRecursosUI {
    
    
      private  DefinirRecursosController uc9_Controller;
    
    public DefinirRecursosUI(CentroExposições cec){
        this.uc9_Controller=new DefinirRecursosController(cec);
    }
    Object recurso =null;

    public void run(){
       
        
        System.out.println("Pf introduza um novo Recurso\n" );
        
      Scanner in=new Scanner(System.in);
    System.out.println("Formulário de Novo Recurso");
    System.out.println("Introduza o nome do novo Recurso\n");
    String nomeRecruso=in.nextLine();
    System.out.println("Introduza a disponibilidade deste recurso");
    float disponibilidadeRecurso=in.nextFloat();
    System.out.println("Introduza o custo  deste recurso");
    float custoRecurso=in.nextFloat();
    
    uc9_Controller.NovoRecurso(nomeRecruso,disponibilidadeRecurso,custoRecurso);
    

      System.out.println("Por favor confirme os dados\n" +uc9_Controller.mostraDados() );
      
      if (Utilitários.confirma("Confirma? (S/N)")) {
            
                System.out.println("Recurso adicionado com sucesso.");
            } else {
                System.out.println("Não foi possivel adicionar corretamente o recurso.");
            }
        
    }  
    
    
    
    
}
