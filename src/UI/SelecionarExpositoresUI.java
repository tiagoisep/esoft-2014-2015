/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.SelecionarExpositoresController;
import Controller.Utilitários;
import Geral.Candidatura;
import Geral.CentroExposições;
import Geral.Exposição;
import java.util.ArrayList;
import java.util.Scanner;



/**
 *
 * @author Joao
 */
public class SelecionarExpositoresUI {
     private final SelecionarExpositoresController uc4_Controller;
    
    public SelecionarExpositoresUI(CentroExposições ce){
        this.uc4_Controller=new SelecionarExpositoresController(ce);
    }
    
    Object exp =null;
    Object candi =null;
    public void run(){
        System.out.println("Lista de exposições das quais é FAE");
        
        ArrayList<Exposição> lisExp = uc4_Controller.getListaExposições();
        System.out.println("Pf selecione uma exposição\n" );

                 exp = Utilitários.apresentaESeleciona(lisExp, "Selecione Exposição");
                 if (exp != null)
                 uc4_Controller.selecionaExposição((Exposição)exp);
         
      
      
      System.out.println("\nLista de candidaturas à exposição selecionada\n");
      
      ArrayList<Candidatura> lisCand = uc4_Controller.getListaCandidaturas();
        do{
       candi = Utilitários.apresentaESeleciona(lisCand, "\nSelecione Candidatura\n");
                 if (candi != null){
                 uc4_Controller.selecionaCandidatura((Candidatura)candi);
                 Scanner in=new Scanner(System.in);
       System.out.println("Qual a decisão?");
       String decisão=in.nextLine();
       System.out.println("Insira um pequena explicaçã");
       String explica=in.nextLine();
       uc4_Controller.NovaDecisão(decisão,explica);
                 }        
     
      
      
       }while(candi != null); 
         System.out.println("PF confirme os dados \n" +uc4_Controller.mostraDados2() +"\n" +uc4_Controller.mostraDados());
          if (Utilitários.confirma("Confirma a decisão sobre esta candidatura? (S/N)")) {
            
                System.out.println("Decisões adicionadas com sucesso.");
            } else {
                System.out.println("Não foi possivel adicionar a decisão à candidatura.");
            }
        }
    
    }
