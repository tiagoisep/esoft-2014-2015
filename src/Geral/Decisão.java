/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Joao
 */
public class Decisão {
    private String decisão;
    private String explicação;
    
     
    private static final String decisão_omissão="Ainda sem decisão";
    private static final String explicação_omissão="Aidna sem explicação";
     
     public Decisão(String decisão){
         this.decisão=decisão;
         this.explicação=explicação;
       
     }
     public Decisão(){
         this.decisão=decisão_omissão;
        this.explicação=explicação_omissão;
     }
     
     public void setDecisão(String decisão){
         this.decisão=decisão;
     }
     public void setExplicação(String explicação){
         this.explicação=explicação;
     }
    
     
     
    @Override
     public String toString(){        
         return "A decisão sobre a candidatura é " +decisão +"\nExplicação : " +explicação;
}
}