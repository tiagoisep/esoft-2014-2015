/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

/**
 *
 * @author Joao
 */
public class Recurso {

    private String nomeRecurso;
    private float disponibilidadeRecurso;
    private float custoRecurso;

    
    private static final String nomeRecurso_omi="Sem nome";
    private static final float disponibilidadeRecurso_omi=0;
    private static final float custoRecurso_omi=0;
    
    public Recurso(String nomeRecurso, float disponibilidadeRecurso,float custoRecurso){
        this.nomeRecurso=nomeRecurso;
        this.disponibilidadeRecurso=disponibilidadeRecurso;
        this.custoRecurso=custoRecurso;
    }
    
    public Recurso(){
        this.nomeRecurso=nomeRecurso_omi;
        this.disponibilidadeRecurso=disponibilidadeRecurso_omi;
        this.custoRecurso=custoRecurso_omi;
    }

    /**
     * @return the nomeRecruso
     */
    public String getNomeRecruso() {
        return nomeRecurso;
    }

    /**
     * @param nomeRecruso the nomeRecruso to set
     */
    public void setNomeRecruso(String nomeRecruso) {
        this.nomeRecurso = nomeRecruso;
    }

    /**
     * @return the disponibilidadeRecurso
     */
    public float getDisponibilidadeRecurso() {
        return disponibilidadeRecurso;
    }

    /**
     * @param disponibilidadeRecurso the disponibilidadeRecurso to set
     */
    public void setDisponibilidadeRecurso(float disponibilidadeRecurso) {
        this.disponibilidadeRecurso = disponibilidadeRecurso;
    }
    
    @Override
    public String toString(){
        return "Recurso " +nomeRecurso +" com a disponiblidade " +disponibilidadeRecurso +" unidades e um custo de " +custoRecurso +" euros";
    }

    /**
     * @return the custoRecurso
     */
    public float getCustoRecurso() {
        return custoRecurso;
    }

    /**
     * @param custoRecurso the custoRecurso to set
     */
    public void setCustoRecurso(float custoRecurso) {
        this.custoRecurso = custoRecurso;
    }

   

    
    
}
