/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class RegistoUtilizadores {
    public  ArrayList<Utilizador> lUtilizadores;
    
    public RegistoUtilizadores(){
        this.lUtilizadores= new ArrayList<>();
        completaUtilizadores();
    }
    
    private void completaUtilizadores(){
            this.lUtilizadores.add(new Utilizador("João","joao@isep.pt","joao123","pass123"));
            this.lUtilizadores.add(new Utilizador("Filipe","filipe@isep.pt","filipe123","pass456"));
            this.lUtilizadores.add(new Utilizador("Tiago","tiago@isep.pt","tiago123","pass789"));
            this.lUtilizadores.add(new Utilizador("Ferreira","ferreira@isep.pt","ferreira123","pass147"));
   }
    
}
