/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import Controller.Utilitários;
import java.util.ArrayList;
import java.util.List;



/**
 *
 * @author Joao
 */
public class CentroExposições {

    public ArrayList<Mecanismos> lMecanismos;
    private RegistoExposições re;
    private RegistoUtilizadores ru;
    private ListaRecursos lr;
 
    
    public CentroExposições(){

        this.lMecanismos= new ArrayList<>();
        this.re=new RegistoExposições ();
        this.ru=new RegistoUtilizadores();
        this.lr=new ListaRecursos();

        completaMecanismos();

    }
   
 


   public void completaMecanismos(){
       this.lMecanismos.add(new Mecanismos("Experiencia"));
       this.lMecanismos.add(new Mecanismos("Aleatório"));
       this.lMecanismos.add(new Mecanismos("Primeiro a chegar"));
   }

  
     private boolean addRecurso(Recurso r)
    {
        return this.lr.lRecurso.add(r);
    }
    public Recurso NovoRecurso() {
        return new Recurso();
    }
 

    public boolean registaExposição(Exposição nova_exposição) {
        return this.re.lExposicoes.add(nova_exposição);
    }
    public boolean registaUtilizador(Utilizador uti){
        return this.ru.lUtilizadores.add(uti);
    }

    public Exposição NovaExposição() {
        return new Exposição();
    }
    public Demonstrações NovaDemonstração(){
        return new Demonstrações();
    }
    public Utilizador NovoUtilizador(){
        return new Utilizador();
    }
   public ArrayList<Utilizador>  getListaUtilizadores(){
      return this.ru.lUtilizadores;
    
   }
   public ArrayList<Exposição> getListaExposições(){
       return this.re.lExposicoes;
   }
    public void addRecursos(Recurso r) {
         Recurso rec = new Recurso();
 
            addRecursoss(rec);
    }

    private boolean addRecursoss(Recurso r)
    {
        return this.lr.lRecurso.add(r);
    }
    
  @Override
    public String toString(){
        String retorno="";
         
        for(int i=0;i<ru.lUtilizadores.size();i++){
         Utilizador uu = ru.lUtilizadores.get(i);
         retorno += uu.toString();
        }
        return retorno;
    }
}
