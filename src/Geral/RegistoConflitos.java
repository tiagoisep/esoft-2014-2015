/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class RegistoConflitos {
    public ArrayList<Conflito> lConflitos;
    
    public RegistoConflitos(){
        this.lConflitos= new ArrayList<>();
        completaConflitos();
    }
    
    
    
     public boolean addConflito(Conflito conflito){
        return lConflitos .contains(conflito)
                ? false
                : lConflitos.add(conflito);
    }
     
    @Override
    public String toString(){
        String s="";
        for(Conflito c:lConflitos){
            s+=c.toString();
        }
        return s;
    }

    public boolean registaConflito(Conflito cc) {
        return this.lConflitos.add(cc);
    }

    private void completaConflitos() {
        this.lConflitos.add(new Conflito("Familia"));
        this.lConflitos.add(new Conflito("Legais"));
    }
    public boolean removeConflito(Conflito cc){
        return this.lConflitos.remove(cc);
    }
}
