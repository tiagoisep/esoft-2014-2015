/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

/**
 *
 * @author Joao
 */
public class Conflito {
    private String tipo;
    
    private final String tipo_omi="sem conflito";
    
    public Conflito(){
        this.tipo=tipo_omi;
    }
    public Conflito(String tipo){
        this.tipo=tipo;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public String toString(){
        return "O tipo de conflito é " +tipo +"\n";
    }
}
