/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

/**
 *
 * @author Joao
 */
public class Utilizador {
    
    private  String nome;
    private  String email;
    private  String username;
    private  String pass;
    
    private static final String nome_omissão="xxxxx";
    private static final String email_omissão="xxxxx";
    private static final String username_omissão="xxxxx";
    private static final String pass_omissão="xxxxx";
    
    public Utilizador(String nome,String email,String username,String pass){
        this.nome=nome;
        this.email=email;
        this.username=username;
        this.pass=pass;
    }
    public Utilizador(){
        this.nome=nome_omissão;
        this.email=email_omissão;
        this.username=username_omissão;
        this.pass=pass_omissão;
    }
    public String getNome(){
        return nome;
    }
    public void setNome(String nome){
        this.nome=nome;
    }
    public void setEmail(String email){
        this.email=email;
    }
    public void setUsername(String username){
        this.username=username;
    }
    public void setPass(String pass){
        this.pass=pass;
    }
    
    
    @Override
    public String toString(){
        return "**********************\nUtilizador com o nome " +nome +"\nEmail " +getEmail() +"\nUsername " +getUsername() +"\nPassword " +getPass() +"\n";
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }
}
