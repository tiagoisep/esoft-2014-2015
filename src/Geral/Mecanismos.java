/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Joao
 */
public class Mecanismos {
    private String nomeMecanismo;
     public  List<Atribuições> LAtribuições;
    
    private static final String nomeMecanismo_omi="Sem nome";
    
    public Mecanismos(String nomeMecanismo){
        LAtribuições=new ArrayList<>();
        this.nomeMecanismo=nomeMecanismo;
       
    }
    public Mecanismos(){
        LAtribuições=new ArrayList<>();
        this.nomeMecanismo=nomeMecanismo_omi;
           }

    /**
     * @return the nomeMecanismo
     */
    public String getNomeMecanismo() {
        return nomeMecanismo;
    }

    /**
     * @param nomeMecanismo the nomeMecanismo to set
     */
    public void setNomeMecanismo(String nomeMecanismo) {
        this.nomeMecanismo = nomeMecanismo;
    }
    
    @Override
    public String toString(){
        return "\nMecanismo " +nomeMecanismo +"\n";
    }
    

    
}
