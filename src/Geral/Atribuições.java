/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class Atribuições {
    
    private Candidatura cand;
    private FAE fae;
    
    public Atribuições(Candidatura cand, FAE fae){
        this.cand=cand;
        this.fae=fae;
        
    }
    public Atribuições(){
        
    }
    
    @Override
    public String toString(){
        return "Candidatura " +cand.toString() +" associada a " +fae.toString() +"\n";
    }
    
    
}
