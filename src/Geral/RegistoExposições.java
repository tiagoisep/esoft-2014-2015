/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class RegistoExposições {
      public  ArrayList<Exposição> lExposicoes;
      
      public RegistoExposições(){
        this.lExposicoes= new ArrayList<>();
        completaExposições();
    }
      
       private void completaExposições(){
       this.lExposicoes.add(new Exposição("Exposição1","É uma exposição teste","Porto","11-11-1111"));
       this.lExposicoes.add(new Exposição("Exposição2","É uma exposição teste","Porto","22-22-2222"));
       this.lExposicoes.add(new Exposição("Exposição3","É uma exposição teste","Porto","33-33-3333"));
       this.lExposicoes.add(new Exposição("Exposição4","É uma exposição teste","Porto","44-44-4444"));
       this.lExposicoes.add(new Exposição("Exposição5","É uma exposição teste","Porto","55-55-5555"));
   }
       public Exposição NovaExposição() {
        return new Exposição();
    }
       public ArrayList<Exposição> getListaExposições(){
       return this.lExposicoes;
   }
       
}
