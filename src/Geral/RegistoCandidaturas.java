/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class RegistoCandidaturas {
     public ArrayList<Candidatura> lCandidaturas;
    
    public RegistoCandidaturas(){
        this.lCandidaturas= new ArrayList<>();
        completaCandidaturas();
    }
    
    private void completaCandidaturas(){
        this.lCandidaturas.add(new Candidatura("Empresa 1","Porto","911 111 111", "111"));
        this.lCandidaturas.add(new Candidatura("Empresa 2","Gaia","922 222 222", "222"));
        this.lCandidaturas.add(new Candidatura("Empresa 3","Maia","933 333 333", "333"));
    }
}
