/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import java.util.List;

/**
 *
 * @author Joao
 */
public class Candidatura {
    private String nomeComercial;
    private String morada;
    private String tlm;
    private String area;
    
    private static final String nomeComercial_omissão="sem nome comercial";
    private static final String morada_omissão="sem morada";
    private static final String tlm_omissão="sem tlm";
    private static final String area_omissão="sem area";
                                    
     
    public Candidatura(String nomeComercial,String morada,String tlm,String area){
        this.nomeComercial=nomeComercial;
        this.morada=morada;
        this.tlm=tlm;
        this.area=area;
    }
    public Candidatura(){
        this.nomeComercial=nomeComercial_omissão;
        this.morada=morada_omissão;
        this.tlm=tlm_omissão;
        this.area=area_omissão;
    }
    public void setNomeComercial(String nomeComercial){
        this.nomeComercial=nomeComercial;
    }
     public void setMorada(String morada){
        this.morada=morada;
    }
      public void settlm (String tlm){
        this.tlm=tlm;
    }
       public void setArea(String area){
        this.area=area;
    }
       
    @Override
       public String toString(){
           return "\nCandidatura com os dados\nNome Comercial - " +nomeComercial +"\nMorada - " +morada +"\nTelemóvel - " +tlm +"\nÁrea de exposiçãopretendida - " +area;
       }
}
