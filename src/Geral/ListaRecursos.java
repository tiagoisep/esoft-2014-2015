/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class ListaRecursos {
    public ArrayList<Recurso> lRecurso;
    
    public ListaRecursos(){
        this.lRecurso= new ArrayList<>();
        completaRecursos();
    }
    
    public void completaRecursos(){
       this.lRecurso.add(new Recurso("Água",100,20));
       this.lRecurso.add(new Recurso("Gás",50,30));
       this.lRecurso.add(new Recurso("TV",20,40));
       this.lRecurso.add(new Recurso("Projetores",3,15));
   }
}
