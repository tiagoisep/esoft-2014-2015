/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Joao
 */
public class Exposição {
    private String titulo;
    private String descritivo;
    private String local;
    private String data;
    public  int contOrganizador ;
    
    private final Atribuições uc_Atribuição;
    public  List<Organizador> lOrganizadores;
    public  List<FAE> lFae;
//    public ArrayList<Candidatura> lCandidaturas;
    public  List<Atribuições> LAtribuições;
    public Demonstrações ucDemosntrações;
    private RegistoCandidaturas ra;
   
    
//    public Exposição(Decisão di){
//        this.uc_decisão=new Decisão(di);
//    }
    public Exposição(){
        this.ucDemosntrações= new Demonstrações();
        this.uc_Atribuição=new Atribuições();
        this.lOrganizadores = new ArrayList<>();
        this.lFae=new ArrayList<>();
//        this.lCandidaturas=new ArrayList<>();
         this.LAtribuições=new ArrayList<>();
         this.ra=new RegistoCandidaturas();
            
//        completaCandidaturas();
    }
    public Exposição(String titulo,String descritivo,String local, String data){
        this.ucDemosntrações= new Demonstrações();
        this.uc_Atribuição=new Atribuições();
        this.titulo=titulo;
        this.descritivo=descritivo;
        this.local=local;
        this.data=data;
        this.lOrganizadores = new ArrayList<>();
        this.lFae=new ArrayList<>();
//        this.lCandidaturas=new ArrayList<>();
         this.LAtribuições=new ArrayList<>();
           this.ra=new RegistoCandidaturas();
//        completaCandidaturas();
    }
    
//    private void completaCandidaturas(){
//        this.lCandidaturas.add(new Candidatura("Empresa 1","Porto","911 111 111", "111"));
//        this.lCandidaturas.add(new Candidatura("Empresa 2","Gaia","922 222 222", "222"));
//        this.lCandidaturas.add(new Candidatura("Empresa 3","Maia","933 333 333", "333"));
//    }
    
    public void setTitulo(String titulo){
        this.titulo=titulo;
    }
    public void setDescritivo(String descritivo){
        this.descritivo=descritivo;
    }
    public void setLocal(String local){
        this.local=local;
    }
    public void setData(String data){
        this.data=data;
    }

    public void addOrganizador(Utilizador u) {
        contOrganizador ++;
         Organizador org = new Organizador();
        org.setUtilizador(u);
            addOrganizador(org);
    }

    private boolean addOrganizador(Organizador o)
    {
        return this.lOrganizadores.add(o);
    }
   
    public void addFae(Utilizador u){
        boolean papel=ValidaPapel(u);
        FAE fae= new FAE();
        fae.setUtilizador(u);
        addFae(fae,papel);
    }
    private boolean addFae(FAE f,boolean papel){
        if(papel== true){
        return this.lFae.add(f);
        }else{
            return false;
        }
    }
    
    
    
    
    public boolean registaCandidatura(Candidatura novaCandidatura) {
        return this.ra.lCandidaturas.add(novaCandidatura);
    }
    @Override
    public String toString(){
        String parametros="";
        parametros="\nTitulo - " +titulo +"\nDescrição - " +descritivo +"\nLocal - " +local +"\nData - " +data +"\n*~*~*~*~*~*~*~*~*~*~*~*~*~*";
        
         for(Organizador org: this.lOrganizadores){
            parametros += String.format("%s \n",org.toString());
        }
         for(FAE fae: this.lFae){
            parametros += String.format("\nFae Associados a esta exposição\n" +fae.toString());
        }
        

         for(Atribuições Atr: this.LAtribuições){
            parametros += String.format("\nFae Associados a esta candidatura\n" +Atr.toString());
         }
         
         
        return parametros;
    }

    public Candidatura NovaCandiatura() {
        return new Candidatura();
    }

    public Decisão NovaDecisão() {
        return new Decisão();
    }
    
    public Demonstrações NovaDemonstração(){
        return new Demonstrações();
    }
    
    public boolean ValidaPapel(Utilizador u){
        return true;
    }
    
    }
    

