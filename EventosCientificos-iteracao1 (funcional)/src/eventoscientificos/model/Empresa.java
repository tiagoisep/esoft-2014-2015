package eventoscientificos.model;

import java.util.*;

/**
 *
 * @author Nuno Silva
 */

public class Empresa
{
    private List<Utilizador> m_listaUtilizadores;
    private List<Evento> m_listaEventos;

    public Empresa()
    {
        m_listaUtilizadores = new ArrayList<Utilizador>();
        m_listaEventos = new ArrayList<Evento>();

        //fillInData();
    }

    public Utilizador novoUtilizador()
    {
        return new Utilizador();
    }
    
    public boolean registaUtilizador(Utilizador u)
    {
        if( u.valida() && validaUtilizador(u) )
            return addUtilizador(u);
        else
            return false;
    }
    
    private boolean validaUtilizador(Utilizador u)
    {
        return true;
    }
    
    public Evento novoEvento()
    {
        return new Evento();
    }

    public boolean registaEvento(Evento e)
    {
        if( e.valida() && validaEvento(e) )
            return addEvento(e);
        else
            return false;
    }
    
    private boolean validaEvento(Evento e)
    {
        return true;
    }

    public Utilizador getUtilizador(String strId)
    {
        for(Utilizador u:this.m_listaUtilizadores)
        {
            String s1 = u.getUsername();
            if(s1.equalsIgnoreCase(strId))
                return u;
        }
        
        /*
        for( Iterator<Utilizador> it = m_listaUtilizadores.listIterator(); it.hasNext(); )
        {
            Utilizador u = it.next();

            if(u.getUsername().equalsIgnoreCase(strId))
                return u;
        }
        */
        
        return null;
    }
    
    private boolean addUtilizador(Utilizador u)
    {
        return m_listaUtilizadores.add(u);
    }

    private boolean addEvento(Evento e)
    {
        return m_listaEventos.add(e);
    }
    
    public List<Evento> getEventosOrganizador(String strId)
    {
        List<Evento> leOrganizador = new ArrayList<Evento>();

        Utilizador u = getUtilizador(strId);
        
        if(u != null )
        {
            for( Iterator<Evento> it = m_listaEventos.listIterator(); it.hasNext(); )
            {
                Evento e = it.next();
                List<Organizador> lOrg = e.getListaOrganizadores();
             
                boolean bRet = false;
                for(Organizador org:lOrg)
                {
                    if (org.getUtilizador().equals(u))
                    {
                        bRet = true;
                        break;
                    }
                }
                if( bRet )
                    leOrganizador.add(e);
            }
        }
        return leOrganizador;
    }
    
    public List<Evento> getListaEventosPodeSubmeter()
    {
        List<Evento> le = new ArrayList<Evento>();
        
        for(Evento e:m_listaEventos)
        {
            if (e.aceitaSubmissoes())
            {
                le.add(e);
            }
        }
        
        return le;
    }
}