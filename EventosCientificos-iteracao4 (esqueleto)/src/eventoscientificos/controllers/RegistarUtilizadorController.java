package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.Utilizador;

/**
 *
 * @author Nuno Silva
 */
public class RegistarUtilizadorController
{
    private Empresa m_empresa;
    private RegistoUtilizadores m_registo;
    private Utilizador m_utilizador;

    public RegistarUtilizadorController(Empresa empresa)
    {
        m_empresa = empresa;
        m_registo = empresa.getRegistoUtilizadores();
    }

    public void novoUtilizador()
    {
        m_utilizador = m_registo.novoUtilizador();
    }

    public Utilizador setDados(String strUsername, String strPassword, String strNome, String strEmail)
    {
        m_utilizador.setUsername(strUsername);
        m_utilizador.setPassword(strPassword);
        m_utilizador.setNome(strNome);
        m_utilizador.setEmail(strEmail);
        
        if( m_registo.registaUtilizador(m_utilizador) )
            return m_utilizador;
        else
            return null;
    }
}

