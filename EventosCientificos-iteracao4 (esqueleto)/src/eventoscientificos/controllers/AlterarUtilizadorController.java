/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Utilizador;

/**
 *
 * @author nunosilva
 */
public class AlterarUtilizadorController
{
    private Empresa m_empresa;
    private Utilizador m_user;
    public AlterarUtilizadorController(Empresa empresa)
    {
        m_empresa = empresa;
    }
 
    public Utilizador getUtilizador(String strID)
    {
        m_user = m_empresa.getRegistoUtilizadores().getUtilizadorByID(strID);
        return m_user;
    }
    
    public boolean alteraDados(String strNome,String strUsername, String strPwd, String strEmail)
    {
       Utilizador uClone = m_user.clone();
       uClone.setNome(strNome);
       uClone.setEmail(strEmail);
       uClone.setUsername(strUsername);
       uClone.setPassword(strPwd);
       return m_empresa.getRegistoUtilizadores().alteraUtilizador(m_user,uClone); 
    }

    
}
