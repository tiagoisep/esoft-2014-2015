/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.ListaRevisoes;
import eventoscientificos.domain.ProcessoDistribuicao;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.Revisao;
import eventoscientificos.domain.Revisavel;
import java.util.List;

/**
 *
 * @author nunosilva
 */
public class ReverArtigoController
{
    private Empresa m_empresa;
    private RegistoEventos m_regEvento;
    private Revisavel m_revisavel;
    private Revisao m_revisao;
    private ListaRevisoes m_listaRevisoes;

    public ReverArtigoController(Empresa empresa)
    {
        m_empresa = empresa;
        m_regEvento = empresa.getRegistoEventos();
    }
    
    public List<Revisavel> getRevisaveisEmRevisaoDoRevisor(String id)
    {
        return m_regEvento.getRevisaveisEmRevisaoDoRevisor(id);
    }
    
    public List<Revisao> selecionaRevisavel(Revisavel r, String id)
    {
        m_revisavel=r;
        ProcessoDistribuicao pd=r.getProcessoDistribuicao();
        m_listaRevisoes=pd.getListaDeRevisoes();
        return m_listaRevisoes.getRevisoesRevisor(id);
        
    }       
            
    public void selecionaRevisao(Revisao rev)
    {
        m_revisao=rev;
    }         
            
    public void setDadosRevisao(String d,String just)
    {
        
        m_revisao.setDecisao(d);
        m_revisao.setJustificacao(just);
        if (m_listaRevisoes.valida(m_revisao))
         m_revisavel.alteraParaEmDecisão();
        
    }         

    
                
}
