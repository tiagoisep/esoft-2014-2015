/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class STStateEmDecisao extends STStateImpl
{

    public STStateEmDecisao(SessaoTematica st)
    {
        super(st);
    }
    
    @Override
    public boolean valida()
    {
        System.out.println("STStateEmDecisao: valida:" + getSessaoTematica().toString());
        return true;
    }

    @Override
    public boolean setStateEmDecisao()
    {
        return true;
    }

    @Override
    public boolean isInEmDecisao()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmDecidido()
    {
        if (valida())
            return getSessaoTematica().setState(new STStateEmDecidido(getSessaoTematica()));
        return false;
    }
    
}
