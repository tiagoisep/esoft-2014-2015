/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class ListaProponentes
{
    private List<Proponente> m_listaProponentes;
    
    public ListaProponentes()
    {
        m_listaProponentes = new ArrayList<Proponente>(); 
    }
    
    public Proponente novoProponente(Utilizador u)
    {
       if (u== null)
           return null;
       System.out.println("ListaProponentes: addProponente: " + u.toString());
       Proponente p = new Proponente(u);
        
       if( p.valida() && validaProponente(p) )
            return p;
        else
            return null;
    }
    
    public boolean registaProponente(Proponente prop)
    {
       
       System.out.println("ListaProponentes: registaProponente: " + prop.toString());
      
       if( prop.valida() && validaProponente(prop) )
            return addProponente(prop);
        else
            return false;
    }
    
    
    private boolean addProponente(Proponente p)
    {
        return m_listaProponentes.add(p);
    }

    private boolean validaProponente(Proponente p)
    {
        System.out.println("ListaProponentes: validaProponente:" + p.toString());
        return true;
    }

    public boolean hasProponente(String strID)
    {
        for(Proponente prop:this.m_listaProponentes)
        {
            if (prop.getUtilizador().getUsername().equals(strID))
                return true;
        }
        return false;
    }
}
