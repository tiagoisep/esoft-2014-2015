/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class MecanismoDetecaoConflito1 implements MecanismoDetecaoConflito
{

    @Override
    public boolean detetarConflito(ProcessoLicitacao pl, Licitacao l, Conflito tc) 
    {
        return false;
    }
    
    @Override
    public String toString()
    {
        return "MecanismoDetecaoConflito1 -> sempre falso";
    }
}
