/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

/**
 *
 * @author Nuno Silva
 */
public class Organizador
{
    private final String m_strNome;
    private Utilizador m_utilizador;

    public Organizador(Utilizador u )
    {
       m_strNome = u.getNome();
       this.setUtilizador(u);
    }

    private void setUtilizador(Utilizador u)
    {
        m_utilizador = u;
    }
    
    public Utilizador getUtilizador()
    {
        return this.m_utilizador;
    }
    
    public boolean valida()
    {
        System.out.println("Organizador:valida: " + this.toString());
        return true;
    }
}
