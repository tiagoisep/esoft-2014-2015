/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

/**
 *
 * @author iazevedo
 */
public interface ProcessoLicitacao {
    Licitacao novaLicitação(Revisor r, Submissao s);

	
    boolean addLicitacao(Licitacao l);

    boolean valida();

    
}
