/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Artigo 
{
    private String m_strTitulo;
    private String m_strResumo;
    private ListaAutores m_listaAutores;
    private Autor m_autorCorrespondente;
    private String m_strFicheiro;
    public Artigo()
    {
        m_listaAutores = new ListaAutores();
    }
    
    public void setTitulo(String strTitulo)
    {
        this.m_strTitulo = strTitulo;
    }
    
    public void setResumo(String strResumo)
    {
        this.m_strResumo = strResumo;
    }
    
    public ListaAutores getListaAutores()
    {
        return this.m_listaAutores;
    }
    
    public List<Autor> getPossiveisAutoresCorrespondentes()
    {
        return this.m_listaAutores.getPossiveisAutoresCorrespondentes();
    }
    
    public void setAutorCorrespondente(Autor autor)
    {
        this.m_autorCorrespondente=autor;
    }
    
     public Autor getAutorCorrespondente()
    {
        return this.m_autorCorrespondente;
    }
    
    public void setFicheiro(String strFicheiro)
    {
        this.m_strFicheiro = strFicheiro;
    }
    
    public String getInfo()
    {
        System.out.println("Artigo:getInfo");
        return "Artigo:getInfo";
    }
    
    public boolean valida()
    {
        System.out.println("Artigo:valida");
        return true;
    }
 

}
