/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class RegistoTipoConflitos
{
    private List<TipoConflito> m_lstTipos;
    
    public RegistoTipoConflitos()
    {
        m_lstTipos = new ArrayList<TipoConflito>();
    }
    
    public TipoConflito novoTipoConflito(String strDescricao, MecanismoDetecaoConflito mecanismo)
    {
        return new TipoConflito(strDescricao,mecanismo);
    }
    
    public boolean registaTipoConflito(TipoConflito tpConflito)
    {
        if (tpConflito.valida() && validaTipoConflito(tpConflito))
            return m_lstTipos.add(tpConflito);
        return false;
    }

    private boolean validaTipoConflito(TipoConflito tpConflito)
    {
        System.out.println("RegistoTipoConflitos: validaTipoConflito: " + tpConflito.toString());
        return true;
    }

    public List<Conflito> getListaConflitos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
