/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class RegistoUtilizadores
{
    private final List<Utilizador> m_listaUtilizadores;
    
    public RegistoUtilizadores()
    {
        m_listaUtilizadores = new ArrayList<Utilizador>();
    }
    
    public Utilizador novoUtilizador()
    {
        return new Utilizador();
    }
    
    public boolean registaUtilizador(Utilizador u)
    {
        if( u.valida() && validaUtilizador(u) )
            return addUtilizador(u);
        else
            return false;
    }
    
    private boolean validaUtilizador(Utilizador u)
    {
        System.out.println("RegistoUtilizadores: validaUtilizador: " + u.toString());
        return true;
    }
    
    private boolean addUtilizador(Utilizador u)
    {
        return m_listaUtilizadores.add(u);
    }
    
    public Utilizador getUtilizadorByID(String strId)
    {
        for(Utilizador u:this.m_listaUtilizadores)
        {
            if (u.getUsername().equals(strId))
                return u;
        }
        return null;
    }
    
    public Utilizador getUtilizadorByEmail(String strEmail)
    {
        for(Utilizador u:this.m_listaUtilizadores)
        {
            if (u.getEmail().equals(strEmail))
                return u;
        }
        return null;
    }

    public boolean alteraUtilizador(Utilizador uOriginal, Utilizador uClone)
    {
        if (uClone.valida())
        {
            List<Utilizador> lstUtilizadores = new ArrayList<Utilizador>(m_listaUtilizadores);
            lstUtilizadores.remove(uOriginal);
            lstUtilizadores.add(uClone);
            if (validaLista(lstUtilizadores))
            {
                uOriginal.setNome(uClone.getNome());
                uOriginal.setEmail(uClone.getEmail());
                uOriginal.setUsername(uClone.getUsername());
                uOriginal.setPassword(uClone.getPwd());
            }
        }
        return false;
    }
    
    private boolean validaLista(List<Utilizador> lista)
    {
        System.out.println("RegistoUtilizadores: validaLista: " + lista.toString());
        return true;
    }
}
