/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 *
 * @author iazevedo
 */
public class ListaSessoesTematicas {
    
    private Evento m_evento;
    private List<SessaoTematica> m_listaST;
    
    public ListaSessoesTematicas(Evento e)
    {
        this.m_evento = e;
        m_listaST = new ArrayList<SessaoTematica>(); 
    }
    
    public List<CPDefinivel> getListaCPDefiniveisEmDefinicaoDoUtilizador(String strID)
    {
        List<CPDefinivel> ls = new ArrayList<CPDefinivel>();
        
        return ls;
    }
    
    public SessaoTematica novaSessaoTematica(String cod, String desc,Date dtInicioSub, Date dtFimSub, Date dtInicioDistr)
    {
        return new SessaoTematica(cod,desc,dtInicioSub,dtFimSub,dtInicioDistr);
    }
    
    public boolean registaSessaoTematica(SessaoTematica st)
    {
        if( st.valida() && validaST(st) )
            return addST(st);
        else
            return false;
    }
    
    private boolean addST(SessaoTematica st)
    {
        if (st.setStateRegistado())
        {
            if (m_listaST.add(st))
            {
                if (this.m_evento.setStateSTDefinidas())
                    return true;
                else
                {
                    m_listaST.remove(st);
                }
            }
        }
        return false;
    }
    
    private boolean validaST(SessaoTematica st)
    {
        System.out.println("ListaSessoesTematicas: validaST:" + st.toString());
        return true;
    }

    public List<Decidivel> getDecisiveis(String strID) {
        List<Decidivel> ld = new ArrayList<>();
        return ld;
    }

    List<Submissivel> getListaSubmissiveisEmSubmissao()
    {
        List<Submissivel> ls = new ArrayList<Submissivel>();
        for(SessaoTematica st:this.m_listaST)
        {
            if (st.isInEmSubmissao())
                ls.add(st);
        }
        return ls;
    }

     List<Revisavel> getRevisaveis(String strID) {
        List<Revisavel> lr = new ArrayList<>();
        return lr;
    }

    
    
    
}
