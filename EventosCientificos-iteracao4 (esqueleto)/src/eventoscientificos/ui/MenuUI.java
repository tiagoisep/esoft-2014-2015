package eventoscientificos.ui;

import eventoscientificos.domain.Empresa;
import java.io.IOException;
import utils.*;

/**
 *
 * @author Nuno Silva
 */
public class MenuUI
{
    private Empresa m_empresa;
    private String opcao;

    public MenuUI(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public void run() throws IOException
    {
        do
        {
            System.out.println("1. Registar utilizador");
            System.out.println("2. Criar evento científico");
            System.out.println("3. Definir CP");
            System.out.println("4. Submeter artigo científico");
            System.out.println("5. Rever artigo");
            System.out.println("6. Criar sessão temática");
            System.out.println("8. Distribuir revisões");
            System.out.println("9. Alterar utilizador");
            System.out.println("10. Alterar submissão de artigo");
            System.out.println("11. Licitar artigos para revisão");
            System.out.println("12. Definir tipo de conflito");
            System.out.println("13. Decidir sobre submissões");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if( opcao.equals("1") )
            {
                RegistarUtilizadorUI ui = new RegistarUtilizadorUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("2") )
            {
                CriarEventoUI ui = new CriarEventoUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("3") )
            {
                DefinirCPUI ui = new DefinirCPUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("4") )
            {
                SubmeterArtigoUI ui = new SubmeterArtigoUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("5") )
            {
                ReverArtigoUI ui = new ReverArtigoUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("6") )
            {
                CriarSessaoTematicaUI ui = new CriarSessaoTematicaUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("8") )
            {
                DistribuirRevisoesUI ui = new DistribuirRevisoesUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("9") )
            {
                AlterarUtilizadorUI ui = new AlterarUtilizadorUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("10") )
            {
                AlterarSubmissaoUI ui = new AlterarSubmissaoUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("11") )
            {
                LicitarArtigoUI ui = new LicitarArtigoUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("12") )
            {
                DefinirTipoConflitoUI ui = new DefinirTipoConflitoUI(m_empresa);
                ui.run();
            }
            else if( opcao.equals("13") )
            {
                DecidirSubmissoesUI ui = new DecidirSubmissoesUI(m_empresa);
                ui.run();
            }

        }
        while (!opcao.equals("0") );
    }
}
