/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import java.util.Date;
import java.util.List;
import utils.Utils;

/**
 *
 * @author nunosilva
 */
class CriarSessaoTematicaUI implements UI
{
    private Empresa m_empresa;
    private CriarSessaoTematicaController m_controller;

    public CriarSessaoTematicaUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controller = new CriarSessaoTematicaController(m_empresa);
    }

    @Override
    public void run()
    {

        String strOrg = Utils.readLineFromConsole("Introduza ID Organizador: ");
        
        List<Evento> lsEventos = m_controller.getListaEventosRegistadosDoUtilizador(strOrg);
        
        Utils.apresentaLista(lsEventos, "Selecione o Evento:");
        Evento e = (Evento) Utils.selecionaObject(lsEventos);
        m_controller.setEvento(e);
        
        String strCodigo = Utils.readLineFromConsole("Introduza Codigo ST: ");
        String strDescr = Utils.readLineFromConsole("Introduza Descricao ST: ");
        Date dtInicioSubmissao = Utils.readDateFromConsole("Introduza Data Inicio Submissao: ");
        Date dtFimSubmissao = Utils.readDateFromConsole("Introduza Data Fim Submissao: ");
        Date dtInicioDistribuicao = Utils.readDateFromConsole("Introduza Data Inicio Distribuicao: ");
        
        m_controller.setDados(strOrg, strDescr, dtInicioSubmissao, dtFimSubmissao, dtInicioDistribuicao);

        
        while (Utils.confirma("Pretende inserir proponente (s/n)?"))
        {
            String strProp = Utils.readLineFromConsole("Introduza ID Proponente: ");
            m_controller.addProponente(strProp);
            if (Utils.confirma("Confirma adicão do proponente?"))
                m_controller.registaProponente();
        }
        
        m_controller.getSessaoTematica().toString();

        if (Utils.confirma("Confirma registo da sessão temática?"))
            m_controller.registaSessaoTematica();
    }
    

}
