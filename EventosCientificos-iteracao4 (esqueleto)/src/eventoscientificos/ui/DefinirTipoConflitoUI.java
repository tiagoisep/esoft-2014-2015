/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.DefinirTipoConflitoController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.MecanismoDetecaoConflito;
import eventoscientificos.domain.TipoConflito;
import java.util.List;
import utils.Utils;

/**
 *
 * @author nunosilva
 */
class DefinirTipoConflitoUI implements UI
{
    private Empresa m_empresa;
    private DefinirTipoConflitoController m_controller;

    public DefinirTipoConflitoUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controller = new DefinirTipoConflitoController(m_empresa);
    }

    @Override
    public void run()
    {
        String strDesc = Utils.readLineFromConsole("Introduza Descrição Tipo Conflito: ");
        List<MecanismoDetecaoConflito> lsObjs = m_empresa.getMecanismosDetecaoConflito();
        Utils.apresentaLista(lsObjs, "Selecione Mecanismo de Detecao:");
        MecanismoDetecaoConflito mec = (MecanismoDetecaoConflito) Utils.selecionaObject(lsObjs);
        
        TipoConflito tc = m_controller.novoTipoConflito(strDesc, mec);
            
        if(Utils.confirma("Confirma (s/n)?"))
            m_controller.registaTipoConflito(tc);
    }
}
