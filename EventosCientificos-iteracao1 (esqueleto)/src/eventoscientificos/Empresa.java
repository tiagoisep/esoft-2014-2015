package eventoscientificos;


import java.util.*;

/**
 *
 * @author Nuno Silva
 */

public class Empresa
{
    private final List<Utilizador> m_listaUtilizadores;
    private List<Evento> m_listaEventos;

    public Empresa()
    {
        m_listaUtilizadores = new ArrayList<Utilizador>();
        m_listaEventos = new ArrayList<Evento>();   
    }

    public Utilizador novoUtilizador()
    {
        return new Utilizador();
    }
    
    public boolean registaUtilizador(Utilizador u)
    {
        if( u.valida() && validaUtilizador(u) )
            return addUtilizador(u);
        else
            return false;
    }
    
    private boolean validaUtilizador(Utilizador u)
    {
        System.out.println("Empresa: validaUtilizador: " + u.toString());
        return true;
    }
    
    private boolean addUtilizador(Utilizador u)
    {
        return m_listaUtilizadores.add(u);
    }
    
    public Utilizador getUtilizador(String strId)
    {
        System.out.println("Empresa:getUtilizador: " + strId);
        return null;
    }

    public Evento novoEvento()
    {
        return new Evento();
    }

    public boolean registaEvento(Evento e)
    {
        System.out.println("Empresa: registaEvento: " + e.toString());
        return e.valida() && validaEvento(e);
    }
    
    private boolean validaEvento(Evento e)
    {
        System.out.println("Empresa: validaEvento:" + e.toString());
        return true;
    }
    
    public List<Evento> getEventosOrganizador(String strId)
    {
        List<Evento> leOrganizador = new ArrayList<Evento>();

        System.out.println("Empresa: getEventosOrganizador: " + strId);
        return leOrganizador;
    }
    
    public List<Evento> getListaEventosPodeSubmeter()
    {
        List<Evento> le = new ArrayList<Evento>();
        
        for(Evento e:m_listaEventos)
        {
            if (e.aceitaSubmissoes())
            {
                le.add(e);
            }
        }
        
        return le;
    }
}